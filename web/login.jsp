<%-- 
    Document   : login
    Created on : Mar 23, 2018, 3:55:20 PM
    Author     : viquy
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Demo login SQL Injection</title>
    </head>
    <body>
        <h1>Hello!</h1>
        <form action="LoginServlet" method="Post">
            <input type ="text" name="username"/><br>
            <input type="password" name="password" />
            <input type="submit" value="Login"/>
        </form> 
    </body>
</html>
