<%-- 
    Document   : salarydetails
    Created on : Mar 23, 2018, 3:55:36 PM
    Author     : viquy
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<c:if test="${sessionScope.username==null}">
    <jsp:forward page="login.jsp"></jsp:forward>
</c:if>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Salary Detail</title>
    </head>
    <body>
        <h1>Hey, ${sessionScope.username} </h1>
        <h2>Your salary in this month is : 0$, goodluck :v</h2>
        <a href="LogoutServlet">Logout</a>
    </body>
</html>
